import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';


@Injectable()
export class TwitterApiService {

  constructor(private http: Http,) { }

  searchcall(searchQuery) {
    const headers = new Headers();
    const searchTerm = 'query=' + searchQuery;
    headers.append('Content-Type', 'application/X-www-form-urlencoded');
    return this.http.post('http://localhost:3000/search', searchTerm, {headers: headers});
  }

  searchUser(userName) {
    const headers = new Headers();
    const searchTerm = 'user_id=' + userName;
    headers.append('Content-Type', 'application/X-www-form-urlencoded');
    return this.http.post('http://localhost:3000/searchUser', searchTerm, {headers: headers});
  }

  getAuthorization() {
    console.log('authorization');
    const headers = new Headers();
    headers.append('Content-Type', 'application/X-www-form-urlencoded');
    return this.http.get('http://localhost:3000/authorization', {headers: headers});
  }

}
