import { SearchQuery } from './seachQuery.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent{
  @Output() searchClicked = new EventEmitter();
  @Output() userSeachClicked = new EventEmitter();

  query: SearchQuery = new SearchQuery();
  constructor() {}

  onSubmit() {
    this.query.searchType === 'username'  ?  this.emitUserSeachClicked() : this.emitSearchClicked();
  }

  emitSearchClicked() {
    console.log('hashtag');
    let query = this.query.search;
    if (this.query.numberOfTweets) {query = query + '&count=' + this.query.numberOfTweets; }
    this.searchClicked.emit(query);
  }

  emitUserSeachClicked() {
    let query = this.query.search;
    if (this.query.numberOfTweets) {query = query + '&count=' + this.query.numberOfTweets; }
    this.userSeachClicked.emit(query);
  }

}
