export class SearchQuery {
  numberOfTweets: number;
  search: string;
  searchType: string;
}
