import {RouterModule} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { TwitterApiService } from './common/services/twitter-api/twitter-api.service';
import { TweetsComponent } from './tweets/tweets.component';
import { HomeComponent } from './home/home.component';

const appRoutes = [
  {path: 'home', component: HomeComponent },
  { path: '',
  redirectTo: '/home',
  pathMatch: 'full'
  },
  { path: 'callback',
  redirectTo: '/home',
  pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    TweetsComponent,
    HomeComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    HttpModule,
    FormsModule,
    BrowserModule,
  ],
  providers: [TwitterApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
