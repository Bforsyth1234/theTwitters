import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { TwitterApiService } from '../common/services/twitter-api/twitter-api.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  public searchquery = '';
  tweets;
  errors;

  constructor(
    private twitterApiService: TwitterApiService,
    private http: Http,
  ) {
    this.authorize();
  }

  authorize() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/X-www-form-urlencoded');
    this.http.post('http://localhost:3000/authorize', { headers: headers }).subscribe((res) => {
      console.log(res);
    });
  }

  onSearchClicked(query) {
    console.log(query);
    this.twitterApiService.searchcall(query).subscribe(results => {
      this.handleResponse(results);
    });
  }

  onUserSeachClicked(userName) {
    this.twitterApiService.searchUser(userName).subscribe(results => {
      this.handleResponse(results);
    });
  }

  handleResponse(results) {
    console.log(results.json().data);
    if (results.json().data.errors) {
      this.errors = results.json().data.errors;
    } else {
      this.tweets = results.json().data.statuses;
    }
  }

}
